#!/usr/bin/env python

import pandas
from matplotlib import pyplot

from common import describe_data
from common import test_env


def main():
    test_env.versions(['matplotlib', 'pandas'])

    df = pandas.read_csv('data/tasutud_maksud_2019_ii_kvartal_eng.csv',
                         sep=';', decimal=',', encoding='latin-1')

    # Print data overview with function print_overview in common/describe_data.py
    print(describe_data.print_overview(df))

    # Print all possible values with counts in column 'County' with help of groupby() and size()
    print(df.groupby('County').size())

    # Print all unique values in column 'Type'
    print(df['Type'].unique())

    # Extract only SME (Small and medium-sized enterprises) data based on number of employees
    sme_df = df[df['Number of employees'] < 250]
    print(sme_df)

    # Extract column 'Number of employees' to variable sme_employees
    sme_employees = sme_df['Number of employees']

    # Calculate mean to variable sme_employees_mean and print the value
    sme_employees_mean = sme_employees.mean()

    # Calculate median to variable sme_employees_median and print the value
    sme_employees_median = sme_employees.median()

    # Calculate mode to variable sme_employees_mode and print the value
    sme_employees_mode = sme_employees.mode()

    # Calculate standard deviation to variable sme_employees_std and print the value
    sme_employees_std = sme_employees.std()

    # Calculate and print quartiles
    sme_employees_quartiles = sme_employees.quantile([0.25, 0.5, 0.75])
    print("Quartiles:\n%s" % sme_employees_quartiles)

    print("Mean, median, mode, std respectively: %d, %d, %d, %d" % (sme_employees_mean,
                                                                    sme_employees_median,
                                                                    sme_employees_mode,
                                                                    sme_employees_std))

    # Draw dataset histogram including mean, median and mode
    figure1 = 'Employees Histogram'
    pyplot.figure(figure1)
    pyplot.hist(sme_employees, range=(sme_employees.min(),
                                      sme_employees.max()), bins=50, edgecolor='black')
    pyplot.title(figure1)
    pyplot.xlabel('Number of Employees')
    pyplot.ylabel('Count')
    pyplot.axvline(sme_employees_mean, color='r', linestyle='solid',
                   linewidth=1, label='Mean')
    pyplot.axvline(sme_employees_median, color='y', linestyle='dotted',
                   linewidth=1, label='Median')
    pyplot.axvline(sme_employees_mode[0], color='orange',
                   linestyle='dashed', linewidth=1, label='Mode')

    # Comment in to see what will happen with histogram when scale is logarithmic
    # plt.yscale('log')
    pyplot.legend()
    pyplot.savefig('results/figure1.png', papertype='a4')

    # Draw boxplot
    figure2 = 'Employees Box Plot'
    pyplot.figure(figure2)
    pyplot.boxplot(sme_employees)
    pyplot.ylabel('Number of Employees')
    pyplot.tick_params(axis='x', which='both',
                       bottom=False, top=False,
                       labelbottom=False)
    pyplot.title(figure2)
    pyplot.savefig('results/figure2.png', papertype='a4')
    pyplot.show()

    # Find and print correlation matrix between 'Number of employees' and 'Labour taxes and payments'
    sme_df2 = sme_df[['Labour taxes and payments',
                      'Number of employees']].corr()
    print(sme_df2)

    # Plot correlation with scatter plot
    figure3 = 'Number of Employees, Labour Taxes and Payments correlation'
    pyplot.figure(figure3)
    pyplot.suptitle(figure3)
    pyplot.subplot(2, 2, 2)
    pyplot.scatter(sme_df2['Number of employees'],
                   sme_df2['Labour taxes and payments'],
                   color='red', s=0.5)
    pyplot.xlabel('Number of Employees')
    pyplot.ylabel('Labour Taxes and Payments')
    pyplot.subplot(2, 2, 3)
    pyplot.scatter(sme_df2['Labour taxes and payments'],
                   sme_df2['Number of employees'],
                   color='red', s=0.5)
    pyplot.ylabel('Number of Employees')
    pyplot.xlabel('Labour Taxes and Payments')
    pyplot.savefig('results/figure3.png', papertype='a4')

    # Show all figures in different windows
    pyplot.show()


if __name__ == '__main__':
    main()
